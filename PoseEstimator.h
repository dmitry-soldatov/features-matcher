#ifndef POSEESTIMATOR_H
#define POSEESTIMATOR_H

#include <ceres/ceres.h>
#include <ceres/rotation.h>
#include <opencv2/opencv.hpp>
#include <memory>

struct ReprojectionError {
    ReprojectionError(const double xObs, const double yObs)
      : xObs(xObs), yObs(yObs)
    { }

    template <typename T>
    bool operator()(const T* const camera,
                    const T* const point, T* residuals) const
    {
        T pointR[3];
        ceres::AngleAxisRotatePoint(camera, point, pointR);

        pointR[0] += T(camera[3]);
        pointR[1] += T(camera[4]);
        pointR[2] += T(camera[5]);

        T xRep = pointR[0] / pointR[2];
        T yRep = pointR[1] / pointR[2];

        residuals[0] = T(xRep) - T(xObs);
        residuals[1] = T(yRep) - T(yObs);

        return true;
    }

    // Factory to hide the construction of the CostFunction object from
    // the client code.
    static ceres::CostFunction* Create(const double xObs, const double yObs)
    {
        return (new ceres::AutoDiffCostFunction<ReprojectionError, 2, 6, 3>(new ReprojectionError(xObs, yObs)));
    }

    double xObs;
    double yObs;
};


struct ReprojectionErrorFixedCam {
    ReprojectionErrorFixedCam(const double xObs, const double yObs, const double* const camera)
      : xObs(xObs), yObs(yObs), camera(camera)
    { }

    template <typename T>
    bool operator()(const T* const point, T* residuals) const
    {
        T pointR[3];

        //very important part!
        T rotation[3];
        rotation[0] = T(camera[0]);
        rotation[1] = T(camera[1]);
        rotation[2] = T(camera[2]);

        ceres::AngleAxisRotatePoint(rotation, point, pointR);

        pointR[0] += T(camera[3]);
        pointR[1] += T(camera[4]);
        pointR[2] += T(camera[5]);

        T xRep = pointR[0] / pointR[2];
        T yRep = pointR[1] / pointR[2];

        residuals[0] = T(xRep) - T(xObs);
        residuals[1] = T(yRep) - T(yObs);

        return true;
    }

    // Factory to hide the construction of the CostFunction object from
    // the client code.
    static ceres::CostFunction* Create(const double xObs, const double yObs, const double* const camera)
    {
        return (new ceres::AutoDiffCostFunction<ReprojectionErrorFixedCam, 2, 3>(new ReprojectionErrorFixedCam(xObs, yObs, camera)));
    }

    double xObs;
    double yObs;
    const double* const camera;
};


struct ReprojectionErrorFixedPoint
{
    ReprojectionErrorFixedPoint(const double xObs, const double yObs, const double* const point)
      : xObs(xObs), yObs(yObs), point(point)
    { }

    template <typename T>
    bool operator()(const T* const camera,
                    T* residuals) const
    {
        T pointR[3];

        T pointSrc[3];
        pointSrc[0] = T(point[0]);
        pointSrc[1] = T(point[1]);
        pointSrc[2] = T(point[2]);

        ceres::AngleAxisRotatePoint(camera, pointSrc, pointR);

        pointR[0] += T(camera[3]);
        pointR[1] += T(camera[4]);
        pointR[2] += T(camera[5]);

        T xRep = pointR[0] / pointR[2];
        T yRep = pointR[1] / pointR[2];

        residuals[0] = T(xRep) - T(xObs);
        residuals[1] = T(yRep) - T(yObs);

        return true;
    }

    // Factory to hide the construction of the CostFunction object from
    // the client code.
    static ceres::CostFunction* Create(const double xObs, const double yObs, const double* const point)
    {
        return (new ceres::AutoDiffCostFunction<ReprojectionErrorFixedPoint, 2, 6>(new ReprojectionErrorFixedPoint(xObs, yObs, point)));
    }

    double xObs;
    double yObs;
    const double* const point;
};



class PoseEstimator
{
public:
    PoseEstimator();

    //static
    void toCeresCamera(cv::Mat const& R, cv::Mat const& T, std::vector<double> &camera);
    void fromCeresCamera(std::vector<double> const &camera, cv::Mat &R, cv::Mat &T);

    void addCameraPairResidualBlocksSecondCamFixed(const std::vector<cv::Point2d> &pLUn, std::vector<double> &cameraL,
                                                   const std::vector<cv::Point2d> &pRUn, std::vector<double> &cameraR, std::vector<std::vector<double>> &P);

    void addCameraPairResidualBlocks(const std::vector<cv::Point2d> &pLUn, std::vector<double> &cameraL,
                                     const std::vector<cv::Point2d> &pRUn, std::vector<double> &cameraR, std::vector<std::vector<double>> &P);

    void addCameraPairResidualBlocksFixedPoints(const std::vector<cv::Point2d> &pLUn, std::vector<double> &cameraL,
                                                    const std::vector<cv::Point2d> &pRUn, std::vector<double> &cameraR, std::vector<std::vector<double>> &P);

    void addCameraPairResidualBlocksFixedPointSecondCamFixed(const std::vector<cv::Point2d> &pLUn, std::vector<double> &cameraL,
                                                    const std::vector<cv::Point2d> &pRUn, std::vector<double> &cameraR, std::vector<std::vector<double>> &P);

    void solve();

    void estimatePointsPosition(const std::vector<cv::Point2d> &pLUn, std::vector<double> &cameraL,
                                const std::vector<cv::Point2d> &pRUn, std::vector<double> &cameraR, std::vector<std::vector<double> > &P);

private:
    std::vector<std::shared_ptr<ceres::CostFunction>> costFunctions;
    ceres::Problem problem;
};

#endif // POSEESTIMATOR_H
