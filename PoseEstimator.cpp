#include "PoseEstimator.h"

PoseEstimator::PoseEstimator()
{

}

void PoseEstimator::solve()
{
    ceres::Solver::Options options;
    options.minimizer_progress_to_stdout = true;
    options.linear_solver_type = ceres::SPARSE_NORMAL_CHOLESKY;
    options.max_num_iterations = 200;
    //options.
    ceres::Solver::Summary summary;
    ceres::Solve(options, &problem, &summary);
   // std::cout << summary.FullReport() << std::endl;

}


void PoseEstimator::toCeresCamera(const cv::Mat &R, const cv::Mat &T, std::vector<double> &camera)
{
    camera.clear();

    cv::Mat Rv;
    cv::Rodrigues(R, Rv);

    for (auto i = 0; i < 3; i++)
        camera.push_back(Rv.at<double>(i));

    for (auto i = 0; i < 3; i++)
        camera.push_back(T.at<double>(i));

#if 0
    std::cout << "camera: ";
    for(auto const &c : camera)
        std::cout << c << " ";
    std::cout << std::endl;
#endif
}


void PoseEstimator::fromCeresCamera(std::vector<double> const &camera, cv::Mat &R, cv::Mat &T)
{
    cv::Mat Rv = cv::Mat(0, 0, CV_64F);
    for (auto i = 0; i < 3; i++)
        Rv.push_back(camera[i]);
    cv::Rodrigues(Rv, R);

    T = cv::Mat(0, 0, CV_64F);
    for (auto i = 3; i < 6; i++)
        T.push_back(camera[i]);
}

void PoseEstimator::estimatePointsPosition(const std::vector<cv::Point2d> &pLUn, std::vector<double> &cameraL,
                                           const std::vector<cv::Point2d> &pRUn, std::vector<double> &cameraR, std::vector<std::vector<double>> &P)
{
    ceres::Problem  lProblem;
    std::vector<std::shared_ptr<ceres::CostFunction>> lCostFunctions;

    std::cout << "Adding " << P.size()*2 << " residual blocks" << std::endl;
    for(int i = 0; i < P.size(); ++i)
    {
        lCostFunctions.push_back(std::shared_ptr<ceres::CostFunction>(ReprojectionErrorFixedCam::Create(pLUn[i].x, pLUn[i].y, cameraL.data())));
        lProblem.AddResidualBlock(costFunctions.back().get(), NULL, P[i].data());
        lCostFunctions.push_back(std::shared_ptr<ceres::CostFunction>(ReprojectionErrorFixedCam::Create(pRUn[i].x, pRUn[i].y, cameraR.data())));
        lProblem.AddResidualBlock(costFunctions.back().get(), NULL, P[i].data());
    }

    ceres::Solver::Options options;
    options.minimizer_progress_to_stdout = true;
    options.linear_solver_type = ceres::SPARSE_NORMAL_CHOLESKY;
    options.max_num_iterations = 200;
    //options.
    ceres::Solver::Summary summary;
    ceres::Solve(options, &lProblem, &summary);
}

void PoseEstimator::addCameraPairResidualBlocksSecondCamFixed(const std::vector<cv::Point2d> &pLUn, std::vector<double> &cameraL,
                                                const std::vector<cv::Point2d> &pRUn, std::vector<double> &cameraR, std::vector<std::vector<double>> &P)
{
    std::cout << "Adding " << P.size()*2 << " residual blocks" << std::endl;
    for(int i = 0; i < P.size(); ++i)
    {
        costFunctions.push_back(std::shared_ptr<ceres::CostFunction>(ReprojectionError::Create(pLUn[i].x, pLUn[i].y)));
        problem.AddResidualBlock(costFunctions.back().get(), NULL, cameraL.data(), P[i].data());
        costFunctions.push_back(std::shared_ptr<ceres::CostFunction>(ReprojectionErrorFixedCam::Create(pRUn[i].x, pRUn[i].y, cameraR.data())));
        problem.AddResidualBlock(costFunctions.back().get(), NULL, P[i].data());
    }
}

void PoseEstimator::addCameraPairResidualBlocks(const std::vector<cv::Point2d> &pLUn, std::vector<double> &cameraL,
                                                const std::vector<cv::Point2d> &pRUn, std::vector<double> &cameraR, std::vector<std::vector<double>> &P)
{
    std::cout << "Adding " << P.size()*2 << " residual blocks" << std::endl;
    for(int i = 0; i < P.size(); ++i)
    {
        costFunctions.push_back(std::shared_ptr<ceres::CostFunction>(ReprojectionError::Create(pLUn[i].x, pLUn[i].y)));
        problem.AddResidualBlock(costFunctions.back().get(), NULL, cameraL.data(), P[i].data());
        costFunctions.push_back(std::shared_ptr<ceres::CostFunction>(ReprojectionError::Create(pRUn[i].x, pRUn[i].y)));
        problem.AddResidualBlock(costFunctions.back().get(), NULL, cameraR.data(), P[i].data());
    }
}

void PoseEstimator::addCameraPairResidualBlocksFixedPoints(const std::vector<cv::Point2d> &pLUn, std::vector<double> &cameraL,
                                                const std::vector<cv::Point2d> &pRUn, std::vector<double> &cameraR, std::vector<std::vector<double>> &P)
{
    std::cout << "Adding " << P.size()*2 << " residual blocks" << std::endl;
    for(int i = 0; i < P.size(); ++i)
    {
        costFunctions.push_back(std::shared_ptr<ceres::CostFunction>(ReprojectionErrorFixedPoint::Create(pLUn[i].x, pLUn[i].y, P[i].data())));
        problem.AddResidualBlock(costFunctions.back().get(), NULL, cameraL.data());
        costFunctions.push_back(std::shared_ptr<ceres::CostFunction>(ReprojectionErrorFixedPoint::Create(pRUn[i].x, pRUn[i].y, P[i].data())));
        problem.AddResidualBlock(costFunctions.back().get(), NULL, cameraR.data());
    }
}



void PoseEstimator::addCameraPairResidualBlocksFixedPointSecondCamFixed(const std::vector<cv::Point2d> &pLUn, std::vector<double> &cameraL,
                                                const std::vector<cv::Point2d> &pRUn, std::vector<double> &cameraR, std::vector<std::vector<double>> &P)
{
    std::cout << "Adding " << P.size()*2 << " residual blocks" << std::endl;
    for(int i = 0; i < P.size(); ++i)
    {
        costFunctions.push_back(std::shared_ptr<ceres::CostFunction>(ReprojectionErrorFixedPoint::Create(pLUn[i].x, pLUn[i].y, P[i].data())));
        problem.AddResidualBlock(costFunctions.back().get(), NULL, cameraL.data());
    }
}
