#include <iostream>

#define DEBUG

#include <fstream>
#include <opencv2/opencv.hpp>
//#include <opencv2/nonfree/nonfree.hpp>
#include <boost/filesystem.hpp>

#include "PoseEstimator.h"

namespace bfs = boost::filesystem;



const static int MIN_DISTANCE = 45; // 45
//const static int MIN_DISTANCE = 60;
static int ERODE_PATCH_SIZE = 60; //shoud be half matcher patch size? // 120

//left camera
/*
    std::vector<double> k_ = {3.3049253611305539e+02, 0., 6.3820964372528385e+02,
                              0., 3.2963170025172070e+02, 4.8310345683908406e+02,
                              0., 0., 1.};
    std::vector<double> d_ = {4.1412039877737665e-02, -1.3287740929531690e-02, 2.2540214432133367e-02, -6.2412385618312687e-03};

    std::vector<double> r_ = {9.9972643941408190e-01, -2.5178363623502556e-03, -2.3253103803467703e-02,
                              1.7474979438441292e-02, -5.8039077868451128e-01,  8.1415058134942875e-01,
                             -1.5545784961476575e-02, -8.1433420935021461e-01, -5.8018800750437127e-01};
    std::vector<double> t_ = {0., 1.0793489289417867e+03, 1.1168557326459404e+03};
*/


//front camera
/*
    std::vector<double> k_ = {3.3479885726695193e+02, 0., 6.3971039453416108e+02,
                              0., 3.3444652875175268e+02, 4.8352936045514571e+02,
                              0., 0., 1.};
    std::vector<double> d_ = {2.4075276311925017e-02, 1.3532353580224227e-02, 5.7695892201978273e-03, -2.6924829891056359e-03};

    std::vector<double> r_ = {-2.0456637457560451e-02, -2.9195283670361938e-01, 9.5621392330620225e-01,
                              -9.9942367394171194e-01, -1.9942858503505100e-02, -2.7470026566424010e-02,
                               2.7089591151968337e-02, -9.5622477667932171e-01, -2.9137661284976224e-01};
    std::vector<double> t_ = {1.8423339246302055e+03, 1.8332469674826172e+01, 5.6535894387752410e+02};
*/


//right camera
/*
    std::vector<double> k_ = {3.3242969879385328e+02, 0., 6.3907602140943118e+02,
                              0., 3.3176034126873145e+02, 4.8314867172982355e+02,
                              0., 0., 1.};
    std::vector<double> d_ = {3.0002506606163697e-02, 5.5880407579596168e-03, 9.6952922922995122e-03, -3.3965195174573723e-03};

    std::vector<double> r_ = {-9.9826847226610316e-01, -5.5376115849554070e-02, -1.9837920075396351e-02,
                              -1.2896616042286840e-02, 5.3509152001997873e-01, -8.4469565081002218e-01,
                              5.7391067024048927e-02, -8.4297719482565237e-01, -5.3487915871691594e-01};
    std::vector<double> t_ = {0., -1.0793489289417867e+03, 1.1256712256915532e+03};
*/


//back camera
/*
    std::vector<double> k_ = {3.3270088599120163e+02, 0., 6.4462638837382599e+02,
                              0., 3.3258431309319457e+02, 4.8217853437046909e+02,
                              0., 0., 1.};
    std::vector<double> d_ = {3.8740844809305612e-02, -4.9511484863922345e-03, 1.5047191288982730e-02, -4.3211132645268819e-03};

    std::vector<double> r_ = {-6.1777955577275966e-02, 4.1655525579360397e-01, -9.0700893219163914e-01,
                               9.9801122010983867e-01, 3.7192700185422319e-02, -5.0895064473766058e-02,
                               1.2533504680002080e-02, -9.0834928409926241e-01, -4.1802450805759450e-01};
    std::vector<double> t_ = {-2.8883213311179447e+03, -1.4089963446080910e+02, 9.4481919710689817e+02};
*/


void loadImages(const std::string &strPathToSequence, std::vector<std::string> &vstrImageFilenames)
{
    bfs::path pathToSequence(strPathToSequence);

    try
    {
        if (bfs::exists(pathToSequence) && bfs::is_directory(pathToSequence))
        {
            std::vector<bfs::path> filePaths;

            std::copy(bfs::directory_iterator(pathToSequence), bfs::directory_iterator(), std::back_inserter(filePaths));
            std::sort(filePaths.begin(), filePaths.end());

            for (auto const &fp : filePaths)
                if(bfs::is_regular_file(fp) && (bfs::extension(fp).compare(".png") == 0))
                    vstrImageFilenames.push_back(fp.string());
        }
        else
            std::cout << pathToSequence << " directory does not exist\n";
    }
    catch (bfs::filesystem_error const &ex)
    {
        std::cout << ex.what() << std::endl;
    }
}


inline int sat(int min, int val, int max)
{
    return val < min ? min : (val > max ? max : val);
}


void decomposeRotationMatrix(const cv::Mat& rotationMatrix, float& phi_x, float& phi_y, float& phi_z)
{
    phi_x = atan2(rotationMatrix.at<double>(2,1), rotationMatrix.at<double>(2,2)); //roll
    phi_y = atan2(-rotationMatrix.at<double>(2,0), sqrt(pow(rotationMatrix.at<double>(2,1),2.0) + pow(rotationMatrix.at<double>(2,2),2.0))); //pitch
    phi_z = atan2(rotationMatrix.at<double>(1,0), rotationMatrix.at<double>(0,0)); //yaw
}


//taken from http://nghiaho.com/?page_id=846
void composeRotationMatrix(const double phi_x, const double phi_y, const double phi_z, cv::Mat& rotationMatrix){
    //create roll matrix

    cv::Mat Rx = cv::Mat::eye(3, 3, CV_64F);
    cv::Mat Ry = cv::Mat::eye(3, 3, CV_64F);
    cv::Mat Rz = cv::Mat::eye(3, 3, CV_64F);

    //fill x matrix
    Rx.at<double>(1,1) = cos(phi_x);
    Rx.at<double>(1,2) = -sin(phi_x);
    Rx.at<double>(2,1) = sin(phi_x);
    Rx.at<double>(2,2) = cos(phi_x);

    //fill y matrix
    Ry.at<double>(0,0) = cos(phi_y);
    Ry.at<double>(0,2) = sin(phi_y);
    Ry.at<double>(2,0) = -sin(phi_y);
    Ry.at<double>(2,2) = cos(phi_y);

    //fill z matrix
    Rz.at<double>(0,0) = cos(phi_z);
    Rz.at<double>(0,1) = -sin(phi_z);
    Rz.at<double>(1,0) = sin(phi_z);
    Rz.at<double>(1,1) = cos(phi_z);

    rotationMatrix = Rz*Ry*Rx;
}


static void pano2persp(const cv::Mat &D, double xi, double yi, double &xo, double &yo)
{
    double *pD = (double *)D.data;

    double th_d = fabs(xi);

    if(th_d > 1e-6)
    {
        double theta = th_d;
        for(int j = 0; j < 10; j++ )
        {
            double theta2 = theta*theta;
            double theta4 = theta2*theta2;
            double theta6 = theta4*theta2;
            double theta8 = theta6*theta2;
            theta = th_d / (1 + pD[0] * theta2 + pD[1] * theta4 + pD[2] * theta6 + pD[3] * theta8);
        }

        double scale = std::tan(theta) / th_d;

        if(std::fabs(180/CV_PI*theta) < 90) //HFOV restriction
        {
            xo = xi * scale;
            yo = yi * scale;
        }
        else
        {
            xo = 0;
            yo = INT_MIN;
        }
    }
    else
    {
        xo = 0;
        yo = INT_MIN;
    }
}


//get "undistort and rotate" map
void getUndistortAndTransformMap(cv::Mat const &imgSrc, cv::Mat const &R, cv::Mat const &T, cv::Mat const &K, cv::Mat const &D, cv::Mat &mapDst)
{
    //build initial map
    cv::Mat mapRUn = cv::Mat(imgSrc.rows, imgSrc.cols, CV_64FC2);
    double *pmapRUn = (double *)mapRUn.data;
    for(int r = 0; r < imgSrc.rows; ++r)
        for(int c = 0; c < imgSrc.cols; ++c)
        {
            double xpan = (c - K.at<double>(2)) / K.at<double>(0);
            double ypan = (r - K.at<double>(5)) / K.at<double>(4);

            double *xpersp = pmapRUn++;
            double *ypersp = pmapRUn++;
#if 1
            pano2persp(D, xpan, ypan, *xpersp, *ypersp);
#else
            *xpersp = xpan;
            *ypersp = ypan;
#endif
        }

    //build transform matrix
    cv::Mat P;
    cv::Mat arow = (cv::Mat_<double>(1,4) << 0, 0, 0, 1);
    cv::hconcat(R.t(), T, P);
    cv::vconcat(P, arow, P);

    //build 3d points
    cv::Mat mapRUn3D = cv::Mat(imgSrc.rows, imgSrc.cols, CV_64FC3);
    double *pmapRUn3D = (double *)mapRUn3D.data;
    double *pmapRUn2 = (double *)mapRUn.data;
    for(int r = 0; r < imgSrc.rows; ++r)
        for(int c = 0; c < imgSrc.cols; ++c)
        {
            *pmapRUn3D++ = *pmapRUn2++;
            *pmapRUn3D++ = *pmapRUn2++;
            *pmapRUn3D++ = 1;
        }

    //rotate undistorted image
    cv::Mat mapUn3D;
    cv::perspectiveTransform(mapRUn3D, mapUn3D, P);

    //build 2d points
    cv::Mat mapUn = cv::Mat(imgSrc.rows, imgSrc.cols, CV_64FC2);
    double *pmapUn = (double *)mapUn.data;
    double *pmapUn3D = (double *)mapUn3D.data;
    for(int r = 0; r < imgSrc.rows; ++r)
        for(int c = 0; c < imgSrc.cols; ++c)
        {
            double x = *pmapUn3D++;
            double y = *pmapUn3D++;
            double z = *pmapUn3D++;

            if(z > 0)
            {
                *pmapUn++ = x / z;
                *pmapUn++ = y / z;
            }
            else
            {
                *pmapUn++ = 0;
                *pmapUn++ = INT_MIN;
            }
        }

    //distort undistorted image
    cv::fisheye::distortPoints(mapUn, mapDst, K, D);
    mapDst.convertTo(mapDst, CV_32FC2);
}


//retain one strongest point per cell
void filterPoints(std::vector<cv::Point2d> &pL, std::vector<cv::Point2d> &pR, int cellSize)
{
    typedef std::pair<int, int> Coords;
    std::map<Coords, cv::Point2d> psMap;

    std::vector<cv::Point2d> pLNew, pRNew;

    for(int i = 0; i < pL.size(); ++i)
    {
        int cx = pL[i].x / cellSize;
        int cy = pL[i].y / cellSize;

        if (psMap.count(Coords(cx, cy)) == 0)
        {
            psMap[Coords(cx, cy)] = pL[i];

            pLNew.push_back(pL[i]);
            pRNew.push_back(pR[i]);
        }
    }

    pL.swap(pLNew);
    pR.swap(pRNew);
}


int filterMatches(std::vector<cv::KeyPoint> const &kpL, std::vector<cv::KeyPoint> const &kpR, std::vector<cv::DMatch> &matches)
{
    std::sort(matches.begin(), matches.end(), [] (cv::DMatch const &a, cv::DMatch const &b) { return a.distance < b.distance; });

    int N = 0;
    for(auto &m : matches)
    {
        if(std::fabs(kpL[m.queryIdx].pt.y - kpR[m.trainIdx].pt.y) > 40)
            m.distance = 1000;
        else if(m.distance < MIN_DISTANCE)
            N++;
    }

    std::sort(matches.begin(), matches.end(), [] (cv::DMatch const &a, cv::DMatch const &b) { return a.distance < b.distance; });

    return N;
}

cv::Ptr<cv::ORB> detector;// = cv::ORB::create(1000, 1.2, 8, 31, 0, 2, cv::ORB::HARRIS_SCORE, 31, 30);
//cv::Ptr<cv::BRISK> detector = cv::BRISK::create(80, 4, 1.2);
cv::Ptr<cv::BFMatcher> matcher;// = cv::BFMatcher::create(cv::NORM_HAMMING, true);
//transform images by map, extract features, search for matches, returns two equal-length vectors of image features
int getMatches(cv::Mat const &imgLsrc, cv::Mat const &mapL, cv::Mat const &roiL,
               cv::Mat const &imgRsrc, cv::Mat const &mapR, cv::Mat const &roiR,
               std::vector<cv::Point2f> &pL, std::vector<cv::Point2f> &pR)
{
    std::vector<cv::KeyPoint> kpL, kpR;
    cv::Mat descR, descL;

    cv::Mat imgL, imgR;
    cv::remap(imgLsrc, imgL, mapL, cv::Mat(), cv::INTER_LINEAR, cv::BORDER_CONSTANT);
    cv::remap(imgRsrc, imgR, mapR, cv::Mat(), cv::INTER_LINEAR, cv::BORDER_CONSTANT);

    detector->detectAndCompute(imgL, roiL, kpL, descL);
    detector->detectAndCompute(imgR, roiR, kpR, descR);

    std::vector<cv::DMatch> matches;
    matcher->match(descL, descR, matches);

    int N = filterMatches(kpL, kpR, matches);

#ifdef DEBUG
    std::vector<char> mask(N, 255);
    mask.resize(matches.size(), 0);

    cv::Mat outImg;
    cv::drawMatches(imgL, kpL, imgR, kpR, matches, outImg, cv::Scalar(0, 200, 0), cv::Scalar::all(0), mask);

    cv::imshow("matches", outImg);
    cv::waitKey(1);
    //cv::imwrite("ex.png", outImg);
#endif

    pL.clear();
    pR.clear();
    for(int j = 0; j < std::min(N, (int)matches.size()); ++j)
    {
        pL.push_back(mapL.at<cv::Point2f>(kpL[matches[j].queryIdx].pt));
        pR.push_back(mapR.at<cv::Point2f>(kpR[matches[j].trainIdx].pt));
    }

/*
    if(pR.size() > 0)
    {
        cv::cvtColor(imgL,imgL,cv::COLOR_RGB2GRAY);
        cv::cvtColor(imgR,imgR,cv::COLOR_RGB2GRAY);
        cv::cornerSubPix(imgL, pL, cv::Size(3, 3), cv::Size(-1, -1), cv::TermCriteria(CV_TERMCRIT_EPS + CV_TERMCRIT_ITER, 20, 0.1));
        cv::cornerSubPix(imgR, pR, cv::Size(3, 3), cv::Size(-1, -1), cv::TermCriteria(CV_TERMCRIT_EPS + CV_TERMCRIT_ITER, 20, 0.1));
    }
*/
    return 0;
}


int getRTByMatches(std::vector<cv::Point2d> const &pL, cv::Mat const &KL, cv::Mat const &DL,
          std::vector<cv::Point2d> const &pR, cv::Mat const &KR, cv::Mat const &DR,
          cv::Mat &R, cv::Mat &T, cv::Mat &inliers)
{
    std::vector<cv::Point2d> pLUn;
    std::vector<cv::Point2d> pRUn;
    cv::fisheye::undistortPoints(pL, pLUn, KL, DL, cv::Mat::eye(3, 3, CV_32F), cv::Mat::eye(3, 3, CV_32F));
    cv::fisheye::undistortPoints(pR, pRUn, KR, DR, cv::Mat::eye(3, 3, CV_32F), cv::Mat::eye(3, 3, CV_32F));

    cv::Mat E = cv::findEssentialMat(pLUn, pRUn, cv::Mat::eye(3, 3, CV_64F), cv::RANSAC, 0.999, 0.002, inliers);

    cv::recoverPose(E, pLUn, pRUn, cv::Mat::eye(3, 3, CV_64F), R, T, inliers);

#ifdef DEBUG
    int cnt = 0;
    for(int i = 0; i < inliers.rows; ++i)
    {
        if(inliers.at<char>(i))
            cnt++;
    }

    std::cout << "Inliers num: " << cnt << std::endl;
#endif

    return 0;
}


int compareRT(cv::Mat const &R1, cv::Mat const &T1, cv::Mat const &R2, cv::Mat const &T2)
{
    std::cout << "------" << std::endl;
    cv::Mat Rdiff = R1.t()*R2;
    float x,y,z;
    decomposeRotationMatrix(Rdiff,x,y,z);
    std::cout << "Angle diff: " << x*180/CV_PI << " " << y*180/CV_PI << " " << z*180/CV_PI << std::endl;

    double scale = cv::norm(T2)/cv::norm(T1);

    double Tdiff = cv::norm(T1, T2);
    std::cout << "Translate diff: " << Tdiff << " (scale: " << 1.0 << ")" << std::endl;

    double TdiffS = cv::norm(T1*scale, T2);
    std::cout << "Translate diff: " << TdiffS << " (scale: " << scale << ")" << std::endl;

    std::cout << "---" << std::endl;
    std::cout << "Talgo (orig): " << T1.t() << std::endl;
    std::cout << "Talgo (scaled): " << T1.t()*scale << std::endl;
    std::cout << "Textr: " << T2.t() << std::endl;
    std::cout << "------" << std::endl;

    return 0;
}


void scaleT(cv::Mat const &T1, cv::Mat &T2)
{
    double scale = cv::norm(T1)/cv::norm(T2);

    T2 *=scale;
}

void getRT(std::vector<std::string> const &imageFilenamesL, cv::Mat const &mapL, cv::Mat const &roiL, cv::Mat const &KL, cv::Mat const &DL,
           std::vector<std::string> const &imageFilenamesR, cv::Mat const &mapR, cv::Mat const &roiR, cv::Mat const &KR, cv::Mat const &DR,
           cv::Mat &R, cv::Mat &T,
           std::vector<cv::Point2d> &pL, std::vector<cv::Point2d> &pR)
{
    std::vector<cv::Point2d> pL_, pR_;
    for(int i = 0; i < std::min(imageFilenamesL.size(), imageFilenamesR.size()); i += 3)
    {
        cv::Mat imgL = cv::imread(imageFilenamesL[i], 1);
        cv::Mat imgR = cv::imread(imageFilenamesR[i], 1);

        std::vector<cv::Point2f> pL__, pR__;
        getMatches(imgL, mapL, roiL, imgR, mapR, roiR, pL__, pR__);

#if 0
        for(auto const &p : pL__)
            cv::circle(imgL, p, 3, cv::Scalar(200,200,0),-1);
        cv::imshow("l",imgL);
        for(auto const &p : pR__)
            cv::circle(imgR, p, 3, cv::Scalar(200,200,0),-1);
        cv::imshow("r",imgR);
#endif

        pL_.insert(pL_.end(), pL__.begin(), pL__.end());
        pR_.insert(pR_.end(), pR__.begin(), pR__.end());
    }

#ifdef DEBUG
    std::cout << "Matches num: " << pR_.size() << std::endl;
#endif

    assert(pL_.size() == pR_.size());

    if(pR_.size() > 0) // > 5
    {
        cv::Mat inliers;
        getRTByMatches(pL_, KL, DL, pR_, KR, DR, R, T, inliers);

        pL.clear();
        pR.clear();
        for(int i = 0; i < inliers.rows; ++i)
        {
            if(inliers.at<char>(i))
            {
                pL.push_back(pL_[i]);
                pR.push_back(pR_[i]);
            }
        }
/*
        if(pL.size() > 200)
        {
            pL.resize(200);
            pR.resize(200);
        }
*/
    }
    else
        std::cout << "not enough matches" << std::endl;
}


void findRT(cv::Mat const &RLR_, cv::Mat const &KL, cv::Mat const &DL, std::vector<std::string> const &imageFilenamesL,
            cv::Mat const &RRL_, cv::Mat const &KR, cv::Mat const &DR, std::vector<std::string> const &imageFilenamesR,
            cv::Mat &R, cv::Mat &T,
            std::vector<cv::Point2d> &pL, std::vector<cv::Point2d> &pR)
{
    cv::Mat img = cv::imread(imageFilenamesL[0]);
    cv::Mat roi = cv::Mat(img.rows, img.cols, 0, 200);

    //form pre processing map: undistort and rotate
    cv::Mat mapL, mapR;
    getUndistortAndTransformMap(img, RLR_, cv::Mat::zeros(3,1,CV_64F), KL, DL, mapL);
    getUndistortAndTransformMap(img, RRL_, cv::Mat::zeros(3,1,CV_64F), KR, DR, mapR);

    //form ROIs
    cv::Mat roiL, roiR;
    cv::remap(roi, roiL, mapL, cv::Mat(), cv::INTER_LINEAR, cv::BORDER_CONSTANT);
    cv::hconcat(cv::Mat::zeros(roiL.rows, roiL.cols/4,0), roiL(cv::Rect(roiL.cols/4, 0, roiL.cols - roiL.cols/4, roiL.rows)), roiL);
    cv::erode(roiL, roiL, cv::Mat::ones(ERODE_PATCH_SIZE,ERODE_PATCH_SIZE,CV_8U), cv::Point(-1,-1), 1);

    cv::remap(roi, roiR, mapR, cv::Mat(), cv::INTER_LINEAR, cv::BORDER_CONSTANT);
    cv::hconcat(roiR(cv::Rect(0, 0, roiR.cols - roiR.cols/4, roiR.rows)), cv::Mat::zeros(roiR.rows, roiR.cols/4,0), roiR);
    cv::erode(roiR, roiR, cv::Mat::ones(ERODE_PATCH_SIZE,ERODE_PATCH_SIZE,CV_8U), cv::Point(-1,-1), 1);

    //compute R,T based on features matches and 5-point algorithm
    getRT(imageFilenamesL, mapL, roiL, KL, DL, imageFilenamesR, mapR, roiR, KR, DR, R, T, pL, pR);
}



double getReprojectionError(cv::Point2d pObs, cv::Mat const& projMatr_, std::vector<double> const &point)
{
    cv::Mat P = (cv::Mat_<double>(4, 1) << point[0], point[1], point[2], 1.0);

    cv::Mat arow = (cv::Mat_<double>(1, 4) << 0, 0, 0, 1.0);
    cv::Mat projMatr;
    cv::vconcat(projMatr_, arow, projMatr);

    cv::Mat pRep = projMatr*P;

    double xRep = pRep.at<double>(0) / pRep.at<double>(2) / pRep.at<double>(3);
    double yRep = pRep.at<double>(1) / pRep.at<double>(2) / pRep.at<double>(3);

    //std::cout << "rep: " << cv::Vec2d(pObs.x, pObs.y) << " " <<  cv::Vec2d(xRep, yRep) << std::endl;
    return cv::norm(cv::Vec2d(pObs.x, pObs.y), cv::Vec2d(xRep, yRep));
}


void trinagulatePoints(const std::vector<cv::Point2d> &pLUn, const cv::Mat &RL, const cv::Mat &TL,
                                      const std::vector<cv::Point2d> &pRUn, const cv::Mat &RR, const cv::Mat &TR,
                                      std::vector<std::vector<double>> &Ps)
{
    cv::Mat projMatrL, projMatrR;
    cv::hconcat(RL, TL, projMatrL);
    cv::hconcat(RR, TR, projMatrR);

    cv::Mat P4Ds;
    cv::triangulatePoints(projMatrL, projMatrR, pLUn, pRUn, P4Ds);

    double totalErrL = 0;
    double totalErrR = 0;
    for(int i = 0; i < pLUn.size(); ++i)
    {
        cv::Mat P4D;
        P4Ds.col(i).copyTo(P4D);
        std::vector<double> P;
        P.push_back(P4D.at<double>(0) / P4D.at<double>(3));
        P.push_back(P4D.at<double>(1) / P4D.at<double>(3));
        P.push_back(P4D.at<double>(2) / P4D.at<double>(3));

        Ps.push_back(P);
#if 1
        totalErrR += getReprojectionError(pLUn[i], projMatrL, P);
        totalErrL += getReprojectionError(pRUn[i], projMatrR, P);
#endif
    }
    std::cout << "errL: " << totalErrL/Ps.size() << " errR: " << totalErrR/Ps.size() << std::endl;
}


#define ENABLE_FR
#define ENABLE_RB
#define ENABLE_BL

int main(int argc, char **argv)
{

    detector = cv::ORB::create(1000, 1.2, 8, 31, 0, 2, cv::ORB::HARRIS_SCORE, 31, 30);
    matcher  = cv::BFMatcher::create(cv::NORM_HAMMING, true);

    cv::Mat TLex = (cv::Mat_<double>(3, 1) << 0., 1.0793489289417867e+03, 1.1168557326459404e+03);
    cv::Mat RLex = (cv::Mat_<double>(3, 3) << 9.9972643941408190e-01, -2.5178363623502556e-03, -2.3253103803467703e-02,
                                            1.7474979438441292e-02, -5.8039077868451128e-01,  8.1415058134942875e-01,
                                           -1.5545784961476575e-02, -8.1433420935021461e-01, -5.8018800750437127e-01);

    cv::Mat TFex = (cv::Mat_<double>(3, 1) << 1.8423339246302055e+03, 1.8332469674826172e+01, 5.6535894387752410e+02);
    cv::Mat RFex = (cv::Mat_<double>(3, 3) << -2.0456637457560451e-02, -2.9195283670361938e-01,  9.5621392330620225e-01,
                                            -9.9942367394171194e-01, -1.9942858503505100e-02, -2.7470026566424010e-02,
                                             2.7089591151968337e-02, -9.5622477667932171e-01, -2.9137661284976224e-01);

    cv::Mat TRex = (cv::Mat_<double>(3, 1) << 0., -1.0793489289417867e+03, 1.1256712256915532e+03);

    cv::Mat RRex = (cv::Mat_<double>(3, 3) << -9.9826847226610316e-01, -5.5376115849554070e-02, -1.9837920075396351e-02,
                                            -1.2896616042286840e-02,  5.3509152001997873e-01, -8.4469565081002218e-01,
                                             5.7391067024048927e-02, -8.4297719482565237e-01, -5.3487915871691594e-01);

    cv::Mat TBex = (cv::Mat_<double>(3, 1) << -2.8883213311179447e+03, -1.4089963446080910e+02, 9.4481919710689817e+02);
    cv::Mat RBex = (cv::Mat_<double>(3, 3) << -6.1777955577275966e-02, 4.1655525579360397e-01, -9.0700893219163914e-01,
                                               9.9801122010983867e-01, 3.7192700185422319e-02, -5.0895064473766058e-02,
                                               1.2533504680002080e-02, -9.0834928409926241e-01, -4.1802450805759450e-01);
#if 0
    {
        float x,y,z;
        decomposeRotationMatrix(RLex, x, y, z);
        std::cout << "Left camera: " << x*180/CV_PI << " " << y*180/CV_PI << " " << z*180/CV_PI << std::endl;
    }

    {
        float x,y,z;
        decomposeRotationMatrix(RFex, x, y, z);
        std::cout << "Front camera: " << x*180/CV_PI << " " << y*180/CV_PI << " " << z*180/CV_PI << std::endl;
    }

    {
        float x,y,z;
        decomposeRotationMatrix(RRex, x, y, z);
        std::cout << "Right camera: " << x*180/CV_PI << " " << y*180/CV_PI << " " << z*180/CV_PI << std::endl;
    }

    {
        float x,y,z;
        decomposeRotationMatrix(RBex, x, y, z);
        std::cout << "Back camera: " << x*180/CV_PI << " " << y*180/CV_PI << " " << z*180/CV_PI << std::endl;
    }
#endif
    cv::Mat DL = (cv::Mat_<double>(1, 4) << 4.1412039877737665e-02, -1.3287740929531690e-02, 2.2540214432133367e-02, -6.2412385618312687e-03);
    cv::Mat KL = (cv::Mat_<double>(3, 3) << 3.3049253611305539e+02, 0., 6.3820964372528385e+02,
                                            0., 3.2963170025172070e+02, 4.8310345683908406e+02,
                                            0., 0., 1.);

    cv::Mat DF = (cv::Mat_<double>(1, 4) << 2.4075276311925017e-02, 1.3532353580224227e-02, 5.7695892201978273e-03, -2.6924829891056359e-03);
    cv::Mat KF = (cv::Mat_<double>(3, 3) << 3.3479885726695193e+02, 0., 6.3971039453416108e+02,
                                            0., 3.3444652875175268e+02, 4.8352936045514571e+02,
                                            0., 0., 1.);

    cv::Mat DR = (cv::Mat_<double>(1, 4) << 3.0002506606163697e-02, 5.5880407579596168e-03, 9.6952922922995122e-03, -3.3965195174573723e-03);
    cv::Mat KR = (cv::Mat_<double>(3, 3) << 3.3242969879385328e+02, 0., 6.3907602140943118e+02,
                                            0., 3.3176034126873145e+02, 4.8314867172982355e+02,
                                            0., 0., 1.);

    cv::Mat DB = (cv::Mat_<double>(1, 4) << 3.8740844809305612e-02, -4.9511484863922345e-03, 1.5047191288982730e-02, -4.3211132645268819e-03);
    cv::Mat KB = (cv::Mat_<double>(3, 3) << 3.3270088599120163e+02, 0., 6.4462638837382599e+02,
                                            0., 3.3258431309319457e+02, 4.8217853437046909e+02,
                                            0., 0., 1.);

    if(argc != 9)
    {
        std::cerr << std::endl << "Usage: ./features-processor l0_sequence f0_sequence f1_sequence r1_sequence r2_sequence b2_sequence b3_sequence l3_sequence" << std::endl;

        return 1;
    }

    std::cout << "======" << std::endl;

    std::vector<std::string> imageFilenamesL0, imageFilenamesF0, imageFilenamesF1, imageFilenamesR1, imageFilenamesR2, imageFilenamesB2, imageFilenamesB3, imageFilenamesL3;
    loadImages(std::string(argv[1]), imageFilenamesL0);
    loadImages(std::string(argv[2]), imageFilenamesF0);
    loadImages(std::string(argv[3]), imageFilenamesF1);
    loadImages(std::string(argv[4]), imageFilenamesR1);
    loadImages(std::string(argv[5]), imageFilenamesR2);
    loadImages(std::string(argv[6]), imageFilenamesB2);
    loadImages(std::string(argv[7]), imageFilenamesB3);
    loadImages(std::string(argv[8]), imageFilenamesL3);

    //L-F pair
    cv::Mat RLF_, RFL_;
    composeRotationMatrix( -30 * CV_PI / 180.0,
                           -45 * CV_PI / 180.0,
                           0 * CV_PI / 180.0, RLF_);
    composeRotationMatrix( -15 * CV_PI / 180.0,
                           45 * CV_PI / 180.0,
                           0 * CV_PI / 180.0, RFL_);

    cv::Mat RLF, TLF;
    std::vector<cv::Point2d> pLLF, pFLF;
    findRT(RLF_, KL, DL, imageFilenamesL0,
           RFL_, KF, DF, imageFilenamesF0,
           RLF, TLF, pLLF, pFLF);

    //comapre result with R,T from extrinsics
    std::cout << "***LF***:" << std::endl;
    if(!RLF.empty() && !TLF.empty())
    {
        cv::Mat RLFex = RFex.t()*RLex;
        cv::Mat TLFex = RFex.t()*(TLex - TFex);

        compareRT(RLF, TLF, RLFex, TLFex);
    }

#ifdef ENABLE_FR
    //F-R pair
    cv::Mat RFR_, RRF_;
    composeRotationMatrix( -15 * CV_PI / 180.0,
                           -45 * CV_PI / 180.0,
                           0 * CV_PI / 180.0, RFR_);
    composeRotationMatrix( -30 * CV_PI / 180.0,
                           45 * CV_PI / 180.0,
                           0 * CV_PI / 180.0, RRF_);

    cv::Mat RFR, TFR;
    std::vector<cv::Point2d> pFFR, pRFR;
    findRT(RFR_, KF, DF, imageFilenamesF1,
           RRF_, KR, DR, imageFilenamesR1,
           RFR, TFR, pFFR, pRFR);

    std::cout << "***FR***:" << std::endl;
    if(!RFR.empty() && !TFR.empty())
    {
        cv::Mat RFRex = RRex.t()*RFex;
        cv::Mat TFRex = RRex.t()*(TFex - TRex);

        compareRT(RFR, TFR, RFRex, TFRex);
    }
#endif

#ifdef ENABLE_RB
    //R-B pair
    cv::Mat RRB_, RBR_;
    composeRotationMatrix( -30 * CV_PI / 180.0,
                           -45 * CV_PI / 180.0,
                           0 * CV_PI / 180.0, RRB_);
    composeRotationMatrix( -25 * CV_PI / 180.0,
                           45 * CV_PI / 180.0,
                           0 * CV_PI / 180.0, RBR_);

    cv::Mat RRB, TRB;
    std::vector<cv::Point2d> pRRB, pBRB;
    findRT(RRB_, KR, DR, imageFilenamesR2,
           RBR_, KB, DB, imageFilenamesB2,
           RRB, TRB, pRRB, pBRB);

    std::cout << "***RB***:" << std::endl;
    if(!RRB.empty() && !TRB.empty())
    {
        cv::Mat RRBex = RBex.t()*RRex;
        cv::Mat TRBex = RBex.t()*(TRex - TBex);

        compareRT(RRB, TRB, RRBex, TRBex);
    }
#endif

#ifdef ENABLE_BL
    //B-L pair
    cv::Mat RBL_, RLB_;
    composeRotationMatrix( -25 * CV_PI / 180.0,
                           -45 * CV_PI / 180.0,
                           0 * CV_PI / 180.0, RBL_);
    composeRotationMatrix( -30 * CV_PI / 180.0,
                           45 * CV_PI / 180.0,
                           0 * CV_PI / 180.0, RLB_);

    cv::Mat RBL, TBL;
    std::vector<cv::Point2d> pBBL, pLBL;
    findRT(RBL_, KB, DB, imageFilenamesB3,
           RLB_, KL, DL, imageFilenamesL3,
           RBL, TBL, pBBL, pLBL);

    std::cout << "***BL***:" << std::endl;
    if(!RBL.empty() && !TBL.empty())
    {
        cv::Mat RBLex = RLex.t()*RBex;
        cv::Mat TBLex = RLex.t()*(TBex - TLex);

        compareRT(RBL, TBL, RBLex, TBLex);
    }
#endif

    std::cout << "======" << std::endl;

#if 1 //BA
    {
        PoseEstimator p;

#if 0 //true extirnsics from camera poses
        TLex = -RLex.t()*TLex;
        RLex = RLex.t();

        TFex = -RFex.t()*TFex;
        RFex = RFex.t();

        TRex = -RRex.t()*TRex;
        RRex = RRex.t();

        TBex = -RBex.t()*TBex;
        RBex = RBex.t();

        cv::Mat RL;
        RLex.copyTo(RL);
        cv::Mat TL;
        TLex.copyTo(TL);

        cv::Mat RF;
        RFex.copyTo(RF);
        cv::Mat TF;
        TFex.copyTo(TF);

        cv::Mat RR;
        RRex.copyTo(RR);
        cv::Mat TR;
        TRex.copyTo(TR);

        cv::Mat RB;
        RBex.copyTo(RB);
        cv::Mat TB;
        TBex.copyTo(TB);

#elif 0 //relative transforamtion from camera poses
        cv::Mat RLFex = RFex.t()*RLex;
        cv::Mat TLFex = RFex.t()*(TLex - TFex);

        cv::Mat RLRex = RRex.t()*RLex;
        cv::Mat TLRex = RRex.t()*(TLex - TRex);

        cv::Mat RLBex = RBex.t()*RLex;
        cv::Mat TLBex = RBex.t()*(TLex - TBex);

        cv::Mat RL = cv::Mat::eye(3, 3, CV_64F);
        cv::Mat TL = cv::Mat::zeros(3, 1, CV_64F);

        cv::Mat RF;
        RLFex.copyTo(RF);
        cv::Mat TF;
        TLFex.copyTo(TF);

        cv::Mat RR;
        RLRex.copyTo(RR);
        cv::Mat TR;
        TLRex.copyTo(TR);

        cv::Mat RB;
        RLBex.copyTo(RB);
        cv::Mat TB;
        TLBex.copyTo(TB);

#else
        cv::Mat RLFex = RFex.t()*RLex;
        cv::Mat TLFex = RFex.t()*(TLex - TFex);

        cv::Mat RLRex = RRex.t()*RLex;
        cv::Mat TLRex = RRex.t()*(TLex - TRex);

        cv::Mat RLBex = RBex.t()*RLex;
        cv::Mat TLBex = RBex.t()*(TLex - TBex);

        cv::Mat RL = cv::Mat::eye(3, 3, CV_64F);
        cv::Mat TL = cv::Mat::zeros(3, 1, CV_64F);

        cv::Mat RF;
        RLF.copyTo(RF);
        cv::Mat TF;
        TLF.copyTo(TF);

        cv::Mat RR = RFR*RF;
        cv::Mat TR = RFR*TF + TFR;

        cv::Mat RB = RRB*RR;
        cv::Mat TB = RRB*TR + TRB;

        scaleT(TLFex, TF);
        scaleT(TLRex, TR);
        scaleT(TLBex, TB);

#endif

        std::cout<< "Befor BA: " << std::endl;
#if 0
        std::cout << "***FL***" << std::endl;
        compareRT(RL, TL, RLex, TLex);
        std::cout << "***FF***" << std::endl;
        compareRT(RF, TF, RFex, TFex);
        std::cout << "***FR***" << std::endl;
        compareRT(RR, TR, RRex, TRex);
        std::cout << "***FB***" << std::endl;
        compareRT(RB, TB, RBex, TBex);
#elif 0
        std::cout << "***FL***" << std::endl;
        compareRT(RL, TL, cv::Mat::eye(3, 3, CV_64F), cv::Mat::zeros(3, 1, CV_64F));
        std::cout << "***FF***" << std::endl;
        compareRT(RF, TF, RLFex, TLFex);
        std::cout << "***FR***" << std::endl;
        compareRT(RR, TR, RLRex, TLRex);
        std::cout << "***FB***" << std::endl;
        compareRT(RB, TB, RLBex, TLBex);
#else
        std::cout << "***FL***" << std::endl;
        compareRT(RL, TL, cv::Mat::eye(3, 3, CV_64F), cv::Mat::zeros(3, 1, CV_64F));
        std::cout << "***FF***" << std::endl;
        compareRT(RF, TF, RLFex, TLFex);
        std::cout << "***FR***" << std::endl;
        compareRT(RR, TR, RLRex, TLRex);
        std::cout << "***FB***" << std::endl;
        compareRT(RB, TB, RLBex, TLBex);
#endif

        std::vector<double> cameraL, cameraF, cameraR, cameraB;
        p.toCeresCamera(RL, TL, cameraL);
        p.toCeresCamera(RF, TF, cameraF);
        p.toCeresCamera(RR, TR, cameraR);
        p.toCeresCamera(RB, TB, cameraB);

        //l-f pair
        std::vector<cv::Point2d> pLUn0;
        std::vector<cv::Point2d> pFUn0;
        cv::fisheye::undistortPoints(pLLF, pLUn0, KL, DL, cv::Mat::eye(3, 3, CV_64F), cv::Mat::eye(3, 3, CV_64F));
        cv::fisheye::undistortPoints(pFLF, pFUn0, KF, DF, cv::Mat::eye(3, 3, CV_64F), cv::Mat::eye(3, 3, CV_64F));

        std::vector<std::vector<double>> PLF;
        trinagulatePoints(pLUn0, RL, TL, pFUn0, RF, TF, PLF);
        p.estimatePointsPosition(pLUn0, cameraL, pFUn0, cameraF, PLF);
        p.addCameraPairResidualBlocksSecondCamFixed(pFUn0, cameraF, pLUn0, cameraL, PLF);

#ifdef ENABLE_FR
        //f-r pair
        std::vector<cv::Point2d> pFUn1;
        std::vector<cv::Point2d> pRUn1;
        cv::fisheye::undistortPoints(pFFR, pFUn1, KF, DF, cv::Mat::eye(3, 3, CV_64F), cv::Mat::eye(3, 3, CV_64F));
        cv::fisheye::undistortPoints(pRFR, pRUn1, KR, DR, cv::Mat::eye(3, 3, CV_64F), cv::Mat::eye(3, 3, CV_64F));

        std::vector<std::vector<double>> PFR;
        trinagulatePoints(pFUn1, RF, TF, pRUn1, RR, TR, PFR);
        p.estimatePointsPosition(pFUn1, cameraF, pRUn1, cameraR, PFR);
        p.addCameraPairResidualBlocks(pFUn1, cameraF, pRUn1, cameraR, PFR);
#endif

#ifdef ENABLE_RB
        //r-b pair
        std::vector<cv::Point2d> pRUn2;
        std::vector<cv::Point2d> pBUn2;
        cv::fisheye::undistortPoints(pRRB, pRUn2, KR, DR, cv::Mat::eye(3, 3, CV_64F), cv::Mat::eye(3, 3, CV_64F));
        cv::fisheye::undistortPoints(pBRB, pBUn2, KB, DB, cv::Mat::eye(3, 3, CV_64F), cv::Mat::eye(3, 3, CV_64F));

        std::vector<std::vector<double>> PRB;
        trinagulatePoints(pRUn2, RR, TR, pBUn2, RB, TB, PRB);
        p.estimatePointsPosition(pRUn2, cameraR, pBUn2, cameraB, PRB);
        p.addCameraPairResidualBlocks(pRUn2, cameraR, pBUn2, cameraB, PRB);
#endif

#ifdef ENABLE_BL
        //b-l pair
        std::vector<cv::Point2d> pBUn3;
        std::vector<cv::Point2d> pLUn3;
        cv::fisheye::undistortPoints(pBBL, pBUn3, KB, DB, cv::Mat::eye(3, 3, CV_64F), cv::Mat::eye(3, 3, CV_64F));
        cv::fisheye::undistortPoints(pLBL, pLUn3, KL, DL, cv::Mat::eye(3, 3, CV_64F), cv::Mat::eye(3, 3, CV_64F));

        std::vector<std::vector<double>> PBL;
        trinagulatePoints(pBUn3, RB, TB, pLUn3, RL, TL, PBL);
        p.estimatePointsPosition(pBUn3, cameraR, pLUn3, cameraL, PBL);
        p.addCameraPairResidualBlocksSecondCamFixed(pBUn3, cameraB, pLUn3, cameraL, PBL);
#endif
        //solve
        std::cout << "camera: ";
        for(auto const &c : cameraF)
            std::cout << c << " ";
        std::cout << std::endl;

        p.solve();

        std::cout << "camera: ";
        for(auto const &c : cameraF)
            std::cout << c << " ";
        std::cout << std::endl;

        p.fromCeresCamera(cameraL, RL, TL);
        p.fromCeresCamera(cameraF, RF, TF);
        p.fromCeresCamera(cameraR, RR, TR);
        p.fromCeresCamera(cameraB, RB, TB);

        std::cout << "88888888888888888888888888" << std::endl;

        {
            std::vector<std::vector<double>> PLF;
            std::vector<std::vector<double>> PFR;
            trinagulatePoints(pLUn0, RL, TL, pFUn0, RF, TF, PLF);
#ifdef ENABLE_FR
            trinagulatePoints(pFUn1, RF, TF, pRUn1, RR, TR, PFR);
#endif
#ifdef ENABLE_RB
            trinagulatePoints(pRUn2, RR, TR, pBUn2, RB, TB, PRB);
#endif
#ifdef ENABLE_BL
            trinagulatePoints(pBUn3, RB, TB, pLUn3, RL, TL, PBL);
#endif
        }

#if 0
        TLex = -RLex.t()*TLex;
        RLex = RLex.t();

        TFex = -RFex.t()*TFex;
        RFex = RFex.t();

        TRex = -RRex.t()*TRex;
        RRex = RRex.t();

        TBex = -RBex.t()*TBex;
        RBex = RBex.t();

        TL = -RL.t()*TL;
        RL = RL.t();

        TF = -RF.t()*TF;
        RF = RF.t();

        TR = -RR.t()*TR;
        RR = RR.t();

        TB = -RB.t()*TB;
        RB = RB.t();

        std::cout << "***FL***" << std::endl;
        compareRT(RL, TL, RLex, TLex);
        std::cout << "***FF***" << std::endl;
        compareRT(RF, TF, RFex, TFex);
        std::cout << "***FR***" << std::endl;
        compareRT(RR, TR, RRex, TRex);
        std::cout << "***FB***" << std::endl;
        compareRT(RB, TB, RBex, TBex);
#elif 0
        std::cout << "***FL***" << std::endl;
        compareRT(RL, TL, cv::Mat::eye(3, 3, CV_64F), cv::Mat::zeros(3, 1, CV_64F));
        std::cout << "***FF***" << std::endl;
        compareRT(RF, TF, RLFex, TLFex);
        std::cout << "***FR***" << std::endl;
        compareRT(RR, TR, RLRex, TLRex);
        std::cout << "***FB***" << std::endl;
        compareRT(RB, TB, RLBex, TLBex);
#else
        std::cout << "***FL***" << std::endl;
        compareRT(RL, TL, cv::Mat::eye(3, 3, CV_64F), cv::Mat::zeros(3, 1, CV_64F));
        std::cout << "***FF***" << std::endl;
        compareRT(RF, TF, RLFex, TLFex);
        std::cout << "***FR***" << std::endl;
        compareRT(RR, TR, RLRex, TLRex);
        std::cout << "***FB***" << std::endl;
        compareRT(RB, TB, RLBex, TLBex);
#endif
    }
#endif
    return 0;
}
